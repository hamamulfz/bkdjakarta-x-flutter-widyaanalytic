import 'package:first/form_validation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ZoomLoginScreen extends StatelessWidget {
  const ZoomLoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            children: [
              Row(
                children: [
                  Icon(Icons.settings),
                  Spacer(),
                  DotIndicator(
                    color: Colors.red,
                  ),
                  DotIndicator(),
                  DotIndicator(),
                  DotIndicator(),
                  DotIndicator(),
                  Spacer(),
                  Opacity(
                    opacity: 0,
                    child: Icon(Icons.settings),
                  )
                ],
              ),
              SizedBox(height: 18),
              Text(
                "Start a Meeting",
                style: TextStyle(
                  fontFamily: "SfPro",
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 18),
              Text("Start or join a video meeting on the go"),
              SizedBox(height: 18),
              Spacer(),
              Image.asset(
                "assets/logo.png",
                width: 250,
              ),
              Spacer(),
              Container(
                width: double.infinity,
                height: 50,
                child: ElevatedButton(
                  onPressed: onPressed,
                  child: Text("Join a meeting"),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 50,
                      child: TextButton(
                        onPressed: () {},
                        child: Text("Sign Up"),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 50,
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => HalamanForm() )
                          );
                        },
                        child: Text("Sign In"),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onPressed() {}
}

class DotIndicator extends StatelessWidget {
  DotIndicator({Key? key, this.color}) : super(key: key);

  Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      height: 10,
      width: 10,
      decoration: BoxDecoration(
        color: color != null ? color : Colors.grey,
        shape: BoxShape.circle,
      ),
    );
  }

  // test(String? text) {
  //   if(text!=null){
  //     return text;
  //   } else {
  //     return "ternyata null";
  //   }

  //   return text ?? "ternyata null";
  //   // if (6 == 9) {
  //   //   return Colors.red;
  //   // } else {
  //   //   return Colors.grey;
  //   // }

  //   // return 6 == 9 ? Colors.red : Colors.grey;
  // }
}
