import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LatihanGestureScreen extends StatefulWidget {
  LatihanGestureScreen({this.judul});
  final String? judul;
  @override
  _LatihanGestureScreenState createState() => _LatihanGestureScreenState();
}

class _LatihanGestureScreenState extends State<LatihanGestureScreen> {
  String text = "belum disentuh";

  Map<String, dynamic>? result;

  fetchData() async {
    String url = "https://jsonplaceholder.typicode.com/posts/1";
    var uri = Uri.parse(url); // mengubah string menjadi
    // parameter utk request get
    final res = await http.get(uri); // untuk membuat request dengan
    // method get
    result = jsonDecode(res.body); //untuk mengubah response
    //string menjadi Map
    setState(() {});
  }

  List list = [];
  fetchDataList() async {
    String url = "https://jsonplaceholder.typicode.com/photos";
    var uri = Uri.parse(url); // mengubah string menjadi
    // parameter utk request get
    final res = await http.get(uri); // untuk membuat request dengan
    // method get
    list = jsonDecode(res.body); //untuk mengubah response
    //string menjadi Map
    print(list.length);
    print(list[0]);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    fetchData();
    fetchDataList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Contoh"),
        ),
        body: list.isEmpty
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView(
                children: List.generate(
                list.length,
                (index) => ListTile(
                  trailing: Image.network(list[index]["thumbnailUrl"]),
                  title: Text(
                    list[index]["title"],
                  ),
                ),
              )
                // [
                //   Text("data 0"),
                //   Text("data 1"),
                //   Text("data 2"),
                //   Text("data 3"),
                //   Text("data 4"),
                // ]
                )
        // Center(
        //   child: Column(
        //     children: [
        //       GestureDetector(
        //         onTap: () {
        //           print("nilai sebelum diubah");
        //           print(text);
        //           // pengubahan nilai
        //           setState(() {});
        //           text = "sudah disentuh";
        //           print("nilai setelah diubah");
        //           print(text);
        //           fetchData();
        //         },
        //         onLongPress: () {
        //           text = "reset";
        //           setState(() {});
        //         },
        //         child: Text(text),
        //       ),
        //       if(result!=null) Text(result!["body"])
        //     ],
        //   ),
        // ),
        );
  }
}
