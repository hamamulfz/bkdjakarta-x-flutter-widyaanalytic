import 'package:flutter/material.dart';

class LatihanScreen extends StatelessWidget {
  const LatihanScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.ac_unit),
            Icon(Icons.ac_unit),
            Icon(Icons.ac_unit),
            Icon(Icons.ac_unit),
            Icon(Icons.ac_unit),
            Icon(Icons.ac_unit),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.ac_unit),
                Icon(Icons.ac_unit),
                Icon(Icons.ac_unit),
                Icon(Icons.ac_unit),
                Icon(Icons.ac_unit),
                OutlinedButton(
                  onPressed: () {},
                  child: Text("OK"),
                )
              ],
            )
          ],
        ));
  }
}

class LatihanKeduaPage extends StatefulWidget {
  const LatihanKeduaPage({Key? key}) : super(key: key);

  @override
  _LatihanKeduaPageState createState() => _LatihanKeduaPageState();
}

class _LatihanKeduaPageState extends State<LatihanKeduaPage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
