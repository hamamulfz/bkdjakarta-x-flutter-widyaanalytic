import 'dart:async';

import 'package:first/form_validation.dart';
import 'package:first/latihan_gesture.dart';
import 'package:first/latihan_screen.dart';
import 'package:first/splash_scree.dart';
import 'package:first/zoom_login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

      // initialRoute: "/",
      // routes: {
      //   '/': (context) => LatihanGestureScreen(),
      //   '/latihan':(context) => LatihanScreen(),
      // },
      // Navigator.pushNamed(context, '/latihan');
      home: SplashScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
   MyHomePage({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final snakeCase = "1";
  final camelCase = 1;
  final _hamburgerCase = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      backgroundColor: Colors.teal,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // Image.network(
              //   "assets/wa-logo.png",
              //   height: 100,
              //   width: 100,
              // ),
              const SizedBox(height: 18),
              const Text(
                'Hamamul Fauzi',
                style: TextStyle(
                    color: Colors.white, fontSize: 30, letterSpacing: 5),
              ),
              const SizedBox(height: 10),
              Text(
                "Developer",
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    ?.copyWith(color: Colors.white, fontSize: 20),
              ),
              SizedBox(height: 18),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 18),
                color: Colors.white,
                child: ListTile(
                  title: Text("+62 812 3456 7890"),
                  subtitle: Text("+62 812 3456 7890"),
                  trailing: Icon(
                    Icons.phone,
                    color: Colors.teal,
                  ),
                  leading: Icon(
                    Icons.phone,
                    color: Colors.teal,
                  ),
                ),
              ),
              SizedBox(height: 18),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 18),
                color: Colors.white,
                child: ListTile(
                  title: Text("hamamul@gmail.com"),
                  leading: const Icon(
                    Icons.mail,
                    color: Colors.teal,
                  ),
                ),
              ),
              SizedBox(height: 18),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 18),
                color: Colors.white,
                child: const ListTile(
                  title: Text(
                    "Aplikasi",
                    textAlign: TextAlign.center,
                  ),
                  // leading: Icon(Icons.phone),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: 
                    (context) => LatihanGestureScreen()
                    )
                  );
                },
                child: Text("Ke halaman latihan"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
